function SuperficieBarrido(curvaCamino, curvaForma, tapaInicial, tapaFinal) {

    this.posicion = vec3.create();
    this.angulosEjes = vec3.create();
    this.escalasEjes = vec3.fromValues(1, 1, 1);
    
    var curvaCamino = curvaCamino;
    var curvaForma = curvaForma;

    var grilla = new VertexGrid();

    grilla.position_buffer = [];
    grilla.color_buffer = [];
    grilla.normal_buffer = [];
    grilla.textura_buffer = [];

    var tapar = function(punto, direccionTangente) { 
        
        for(var i = 0; i<curvaForma.puntos.length; i++) {

            grilla.position_buffer.push(punto[0]);
            grilla.position_buffer.push(punto[1]);
            grilla.position_buffer.push(punto[2]);
            grilla.normal_buffer.push(punto[0]);
            grilla.normal_buffer.push(punto[1]);
            grilla.normal_buffer.push(punto[2]);
            grilla.color_buffer.push(1.0);
            grilla.color_buffer.push(0.2);
            grilla.color_buffer.push(1.0);

        }

    }

    this.dibujar = function(matrizTransformacion, line) {

        var localMatrizTransformacion = mat4.create();
        var localviewMatriz = mat4.create();
        mat4.copy(localviewMatriz, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);

        if(line) {
            grilla.drawLine(localMatrizTransformacion);
        } else {
            grilla.draw(localMatrizTransformacion, localviewMatriz);
        }

    }

    grilla.createIndexBuffer(curvaCamino.puntos.length + 1*tapaInicial + 1*tapaFinal, curvaForma.puntos.length);

    if(tapaInicial) {
        tapar(curvaCamino.puntos[0], -1.0);
    }

    for(var i = 0; i<curvaCamino.puntos.length; i++) {

        var curvaCaminoBinormal = vec3.create();
        vec3.cross(curvaCaminoBinormal, curvaCamino.tangentes[i], curvaCamino.normales[i]);
        vec3.normalize(curvaCaminoBinormal, curvaCaminoBinormal);

        var matrizTraslacion = mat4.create();
        mat4.translate(matrizTraslacion, matrizTraslacion, curvaCamino.puntos[i]);

        var matrizRotacion = mat3.create();
        for (var j = 0; j<3; j++) {
            matrizRotacion[j + 0] = curvaCaminoBinormal[j];
            matrizRotacion[j + 3] = curvaCamino.normales[i][j];
            matrizRotacion[j + 6] = curvaCamino.tangentes[i][j];
        }

        for(var j = 0; j<curvaForma.puntos.length; j++) {

            var puntoCurvaForma = vec3.create();
            vec3.copy(puntoCurvaForma, curvaForma.puntos[j]);

            var normalPuntoCurvaForma = vec3.create();
            vec3.copy(normalPuntoCurvaForma, curvaForma.normales[j]);

            vec3.transformMat3(puntoCurvaForma, puntoCurvaForma, matrizRotacion);
            vec3.transformMat4(puntoCurvaForma, puntoCurvaForma, matrizTraslacion);

            vec3.transformMat3(normalPuntoCurvaForma, normalPuntoCurvaForma, matrizRotacion);

            grilla.position_buffer.push(puntoCurvaForma[0]);
            grilla.position_buffer.push(puntoCurvaForma[1]);
            grilla.position_buffer.push(puntoCurvaForma[2]);
            grilla.normal_buffer.push(puntoCurvaForma[0]);
            grilla.normal_buffer.push(puntoCurvaForma[1]);
            grilla.normal_buffer.push(puntoCurvaForma[2]);
            grilla.color_buffer.push(1.0/curvaCamino.length * i);
            grilla.color_buffer.push(0.2);
            grilla.color_buffer.push(1.0/curvaForma.length * j);

        }

    }

    if(tapaFinal) {
        tapar(curvaCamino.puntos[curvaCamino.puntos.length - 1], 1.0);
    }

    grilla.setupWebGLBuffers();
    

}
