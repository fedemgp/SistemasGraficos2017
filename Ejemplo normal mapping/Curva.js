function Curva (tipo, puntosControl, cantidadPuntosPorSegmento) {

    this.puntos = [];
    this.normales = [];
    this.tangentes = [];
    this.posicion = vec3.create();
    this.angulosEjes = vec3.create();
    this.escalasEjes = vec3.fromValues(1, 1, 1);

    var grilla = new VertexGrid();

    grilla.position_buffer = [];
    grilla.color_buffer = [];
    grilla.normal_buffer = [];
    grilla.textura_buffer = [];
    
    var puntosControl = puntosControl;
    var deltaU = 1/(cantidadPuntosPorSegmento-1);
    var avance = null;
    var cantidadSegmentos = null;
    var Base0, Base1, Base2, Base3;
    var Base0der, Base1der, Base2der, Base3der;

    if(tipo === "bezier" || tipo === null) {
        Base0=function(u) { return (1-u)*(1-u)*(1-u); }  // 1*(1-u) - u*(1-u) = 1-2u+u2  ,  (1-2u+u2) - u +2u2- u3 ,  1 - 3u +3u2 -u3

        Base1=function(u) { return 3*(1-u)*(1-u)*u; }

        Base2=function(u) { return 3*(1-u)*u*u; }

        Base3=function(u) { return u*u*u; }

        // bases derivadas

        Base0der=function(u) { return -3*u*u+6*u-3; } //-3u2 +6u -3

        Base1der=function(u) { return 9*u*u-12*u+3; }  // 9u2 -12u +3

        Base2der=function(u) { return -9*u*u+6*u; }		 // -9u2 +6u

        Base3der=function(u) { return 3*u*u; }			// 3u2

        cantidadSegmentos = (puntosControl.length-1)/3;
        avance = 3;
    } else if(tipo === "bspline") {
        Base0=function(u) { return (1-3*u+3*u*u-u*u*u)*1/6; }  // (1 -3u +3u2 -u3)/6
        
        Base1=function(u) { return (4-6*u*u+3*u*u*u)*1/6; }  // (4  -6u2 +3u3)/6

        Base2=function(u) { return (1+3*u+3*u*u-3*u*u*u)*1/6; } // (1 +3u +3u2 -3u3)/6

        Base3=function(u) { return (u*u*u)*1/6; }  //    u3/6


        Base0der=function(u) { return (-3 +6*u -3*u*u)/6; }  // (-3 +6u -3u2)/6

        Base1der=function(u) { return (-12*u+9*u*u)/6; }   // (-12u +9u2)  /6

        Base2der=function(u) { return (3+6*u-9*u*u)/6; }	// (-3 +6u -9u2)/6

        Base3der=function(u) { return (3*u*u)*1/6; }

        cantidadSegmentos = puntosControl.length-3;
        avance = 1;
    } else {
        return null;
    }

    this.en = function(u, p0, p1, p2, p3) {

        var punto = vec3.create();
        
        punto[0] = Base0(u)*p0[0] + Base1(u)*p1[0] + Base2(u)*p2[0] + Base3(u)*p3[0];
        punto[1] = Base0(u)*p0[1] + Base1(u)*p1[1] + Base2(u)*p2[1] + Base3(u)*p3[1];
        punto[2] = 0.0;

        return punto;

    }

    this.tangenteEn = function(u, p0, p1, p2, p3) {

        var tangente = vec3.create();
        
        tangente[0] = Base0der(u)*p0[0] + Base1der(u)*p1[0] + Base2der(u)*p2[0] + Base3der(u)*p3[0];
        tangente[1] = Base0der(u)*p0[1] + Base1der(u)*p1[1] + Base2der(u)*p2[1] + Base3der(u)*p3[1];
        tangente[2] = 0.0;
        vec3.normalize(tangente, tangente);

        return tangente;

    }
    
    this.normalEn = function(u, p0, p1, p2, p3) {

        var normal = this.tangenteEn(u, p0, p1, p2, p3);
        vec3.rotateZ(normal, normal, vec3.create(), Math.PI/2);
        vec3.normalize(normal, normal);

        return normal;

    }

    this.dibujar = function(matrizTransformacion) {
        
        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);        
        
        grilla.drawLine(localMatrizTransformacion);

    }

    this.dibujarNormales = function(matrizTransformacion) {

        var grillaNormales = new VertexGrid();

        grillaNormales.position_buffer = [];
        grillaNormales.color_buffer = [];
        grillaNormales.normal_buffer = [];
        grillaNormales.textura_buffer = [];

        for(var i = 0; i<this.puntos.length; i++) {
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+1]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+2]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3] + grilla.normal_buffer[i*3]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+1] + grilla.normal_buffer[i*3+1]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+2] + grilla.normal_buffer[i*3+2]);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(1);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(1);
        }
        
        grillaNormales.createIndexBuffer(1, 2*this.puntos.length);
        grillaNormales.setupWebGLBuffers();
        
        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);

        grillaNormales.drawLine(localMatrizTransformacion, true);

    }

    var posicion = 0;
    var evitarRepeticionPunto = false;
    
    for(var i = 0; i<cantidadSegmentos; i++) {

        var p0 = puntosControl[posicion];
        var p1 = puntosControl[posicion+1];
        var p2 = puntosControl[posicion+2];
        var p3 = puntosControl[posicion+3];

        posicion += avance;

        if(i !== 0) evitarRepeticionPunto = true;
       
        for(var u = 0.0 + evitarRepeticionPunto*deltaU; u<=1.001; u += deltaU) {

            this.puntos.push(this.en(u, p0, p1, p2, p3));
            this.normales.push(this.normalEn(u, p0, p1, p2, p3));
            this.tangentes.push(this.tangenteEn(u, p0, p1, p2, p3));

        }

    }

    grilla.createIndexBuffer(1, this.puntos.length);
    
    for(var i = 0; i<this.puntos.length; i++) {
        grilla.position_buffer.push(this.puntos[i][0]);
        grilla.position_buffer.push(this.puntos[i][1]);
        grilla.position_buffer.push(this.puntos[i][2]);
        grilla.color_buffer.push(0.1);
        grilla.color_buffer.push(0.1);
        grilla.color_buffer.push(1.0);
        grilla.normal_buffer.push(this.normales[i][0]);
        grilla.normal_buffer.push(this.normales[i][1]);
        grilla.normal_buffer.push(this.normales[i][2]);
    }

    grilla.setupWebGLBuffers();


}
