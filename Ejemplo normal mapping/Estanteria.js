function Estanteria(estantesX, estantesY) {

    const ANCHO_ESTANTES = 1;
    const PROFUNDIDAD_ESTANTES = 1;
    const ALTO_ESTANTES = 1;
    const ALTO_BASE = 2;
    const PROFUNDIDAD_BASE = PROFUNDIDAD_ESTANTES;
    const ANCHO_TABIQUES = PROFUNDIDAD_ESTANTES;
    const PROFUNDIDAD_TABIQUES = 0.3;
    const ALTO_TABIQUES_X = ANCHO_ESTANTES;
    const ALTO_TABIQUES_Y = estantesY * ANCHO_ESTANTES + (estantesY - 1) * PROFUNDIDAD_TABIQUES;
    const ALTO_TECHO = estantesX * ANCHO_ESTANTES + (estantesX + 1) * PROFUNDIDAD_TABIQUES;
    const ANCHO_BASE = ALTO_TECHO;
    const PROFUNDIDAD_TECHO = PROFUNDIDAD_TABIQUES;
    const ANCHO_TECHO = PROFUNDIDAD_ESTANTES;

    this.posicion = vec3.create();
    this.angulosEjes = vec3.create();
    this.escalasEjes = vec3.fromValues(1, 1, 1);

    var tabiquesX = estantesX * (estantesY - 1);
    var tabiquesY = estantesX + 1;
    
    var hijos = [];

    for(var i = 0; i<tabiquesY; i++) {

        var tabiqueY = new Prisma(ANCHO_TABIQUES, PROFUNDIDAD_TABIQUES, ALTO_TABIQUES_Y, true, true);
        tabiqueY.posicion = [
            -ALTO_TECHO/2 + PROFUNDIDAD_TABIQUES/2 + i * (ANCHO_ESTANTES + PROFUNDIDAD_TABIQUES),
            ALTO_TABIQUES_Y/2 + ALTO_BASE,
            0
        ];
        tabiqueY.angulosEjes = [0, -Math.PI/2, 0];

        hijos.push(tabiqueY);

    }
    
    for(var i = 0; i<tabiquesX; i++) {
        
        var columna = i % estantesX;
        var fila = Math.trunc(i/estantesX);

        var tabiqueX = new Prisma(ANCHO_TABIQUES, PROFUNDIDAD_TABIQUES, ALTO_TABIQUES_X, true, true);
        tabiqueX.posicion= [
            -ALTO_TECHO/2 + PROFUNDIDAD_TABIQUES + (ANCHO_ESTANTES + PROFUNDIDAD_TABIQUES) * columna + ANCHO_TABIQUES/2,
            ALTO_BASE + (ALTO_ESTANTES + PROFUNDIDAD_TABIQUES/2) * (fila + 1) + PROFUNDIDAD_TABIQUES/2 * fila,
            0
        ];
        tabiqueX.angulosEjes = [-Math.PI/2, 0, 0];

        hijos.push(tabiqueX);

    }

    var techo = new Prisma(ANCHO_TECHO, PROFUNDIDAD_TECHO, ALTO_TECHO, true, true);
    techo.posicion = [0, ALTO_BASE + ALTO_TABIQUES_Y + PROFUNDIDAD_TECHO/2, 0];
    techo.angulosEjes = [-Math.PI/2, 0, -Math.PI/2];

    hijos.push(techo);
    
    var base = new Prisma(ANCHO_BASE, PROFUNDIDAD_BASE, ALTO_BASE, true, true);
    base.posicion = [0, ALTO_BASE/2, 0];

    hijos.push(base);

    this.dibujar = function(matrizTransformacion) {

        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);

        hijos.forEach( function(elemento) {
            elemento.dibujar(localMatrizTransformacion);
        });

    }

}
