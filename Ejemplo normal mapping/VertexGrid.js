function VertexGrid () {

    this.index_buffer = null;

    this.position_buffer = null;
    this.color_buffer = null;
    this.normal_buffer = null;
    this.tangent_buffer = null;
    this.textura_buffer = null;

    this.webgl_position_buffer = null;
    this.webgl_color_buffer = null;
    this.webgl_normal_buffer = null;
    this.webgl_tangent_buffer = null;
    this.webgl_textura_buffer = null;
    this.webgl_index_buffer = null;

    this.texturas = null;

    this.createIndexBuffer = function(rows, cols){

        this.index_buffer = [];
        
        if(rows === 1) {
            for(var i = 0; i<cols; i++) {
                this.index_buffer.push(i);
            }
        } else {
            for (var i = 0; i<(rows-1); i++) {
                
                if(i != 0 && i & 1) this.index_buffer.push(this.index_buffer[this.index_buffer.length - 1]);

                if (!(i & 1)) { // even rows
                    for (var j = 0; j<cols; j++) {
                        this.index_buffer.push(j + i * cols);
                        this.index_buffer.push(j + (i + 1) * cols);
                    }
                } else { // odd rows
                    for (var j = (cols-1); j>0; j--) {
                        this.index_buffer.push(j + (i + 1) * cols);
                        this.index_buffer.push(j - 1 + i * cols);
                    }
                }

                if(i & 1) this.index_buffer.push((i + 1) * cols);

            }
        }

    }

    this.crearNormalBuffer = function() {

        this.normal_buffer = [];
        var normales_triangulos = [];
        var columna = 0;

        // Calculo las normales de todos los triángulos. Para obtener cada triángulo
        // recorro el index buffer tomando de a 3 vértices y salteando los repetidos
        for(var i = 0; (i + 2)<this.index_buffer.length; i++) {
            
            // Salteo cuando se repite y eso indica cambio de columna
            if(this.index_buffer[i+1]==this.index_buffer[i+2]) {

                i++;
                columna++;

            } else{

                var v1 = this.obtenerVector(this.position_buffer, this.index_buffer[i]);
                var v2 = this.obtenerVector(this.position_buffer, this.index_buffer[i + 1]);
                var v3 = this.obtenerVector(this.position_buffer, this.index_buffer[i + 2]);
                var normal_triangulo = this.calcularNormal(v1, v2, v3);
                // La normal va alternando el sentido de uno a otro y cuando se cambia de columna
                // el "orden" en que alternan cambia
                if((i+columna)&1) {
                    vec3.negate(normal_triangulo, normal_triangulo);
                }
                normales_triangulos.push(normal_triangulo[0]);
                normales_triangulos.push(normal_triangulo[1]);
                normales_triangulos.push(normal_triangulo[2]);

            }

        }

        var cantidad_vertices = this.position_buffer.length/3;
        var epsilon = 0.00001;

        // Para cada vértice me fijo los triángulos en los que está presente recorriendo el
        // index buffer igual que antes y sumo las normales
        for(var i = 0; i<cantidad_vertices; i++) {

            var normal_vertice = vec3.create();
            // Voy contando los triángulos para poder obtener sus normales cuando sea necesario
            var triangulo = 0;

            for(var j = 0; (j + 2)<this.index_buffer.length; j++) {

                if(this.index_buffer[j+1]==this.index_buffer[j+2]) {

                    j++;

                } else{
                    var vertice = this.obtenerVector(this.position_buffer, i);
                    if(vec3.squaredDistance(vertice, this.obtenerVector(this.position_buffer, this.index_buffer[j]))<epsilon || vec3.squaredDistance(vertice, this.obtenerVector(this.position_buffer, this.index_buffer[j+1]))<epsilon || vec3.squaredDistance(vertice, this.obtenerVector(this.position_buffer, this.index_buffer[j+2]))<epsilon) {
                        vec3.add(normal_vertice, normal_vertice, this.obtenerVector(normales_triangulos, triangulo));
                    }

                    triangulo++;

                }

            }

            // Normalizo el resultado
            vec3.normalize(normal_vertice, normal_vertice);

            this.normal_buffer.push(normal_vertice[0]);
            this.normal_buffer.push(normal_vertice[1]);
            this.normal_buffer.push(normal_vertice[2]);

        }

    }

    this.obtenerVector = function(arreglo, numVector) {

        var vector = vec3.fromValues(
            arreglo[numVector*3],
            arreglo[numVector*3 + 1],
            arreglo[numVector*3 + 2]
        )

        return vector;

    }

    // Tomo los vectores v2-v1 y v3-v1, calculo el producto vectorial
    // y devuelvo el resultado normalizado
    this.calcularNormal = function(v1, v2, v3) {
        
        var normal = vec3.create();
        var direccion_v1_v2 = vec3.create();
        var direccion_v1_v3 = vec3.create();
        var direccion_normal = vec3.create();
        
        vec3.sub(direccion_v1_v2, v2, v1);
        vec3.sub(direccion_v1_v3, v3, v1);
        vec3.cross(direccion_normal, direccion_v1_v2, direccion_v1_v3);
        vec3.normalize(normal, direccion_normal);

        return normal;

    }

    // Esta función crea e incializa los buffers dentro del pipeline para luego
    // utlizarlos a la hora de renderizar.
    this.setupWebGLBuffers = function() {

        // 1. Creamos un buffer para las posicioens dentro del pipeline.
        this.webgl_position_buffer = gl.createBuffer();
        // 2. Le decimos a WebGL que las siguientes operaciones que vamos a hacer se aplican sobre el buffer que
        // hemos creado.
        gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_position_buffer);
        // 3. Cargamos datos de las posiciones en el buffer.
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.position_buffer), gl.STATIC_DRAW);

        this.webgl_color_buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_color_buffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.color_buffer), gl.STATIC_DRAW);

        if(this.textura_buffer) {
            this.webgl_textura_buffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_textura_buffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.textura_buffer), gl.STATIC_DRAW);
        }

        if(this.normal_buffer) {
            this.webgl_normal_buffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_normal_buffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.normal_buffer), gl.STATIC_DRAW);
        }

        if(this.tangent_buffer) {
            this.webgl_tangent_buffer = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_tangent_buffer);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.tangent_buffer), gl.STATIC_DRAW);
        }

        this.webgl_index_buffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.webgl_index_buffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.index_buffer), gl.STATIC_DRAW);

    }

    this.draw = function(matrizTransformacion, viewMatriz) {

        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);

        var nMatrix = mat3.create();
        var u_model_view_matrix = gl.getUniformLocation(glProgram, "umvMatrix");
        var u_normal_matrix = gl.getUniformLocation(glProgram, "uNMatrix");
        var u_model_matrix = gl.getUniformLocation(glProgram, "umMatrix");
        mat3.normalFromMat4(nMatrix, localMatrizTransformacion);
        gl.uniformMatrix3fv(u_normal_matrix, false, nMatrix);
        gl.uniformMatrix4fv(u_model_matrix, false, localMatrizTransformacion);
        if(viewMatriz) {
            mat4.multiply(localMatrizTransformacion, viewMatriz, localMatrizTransformacion);
        }
        gl.uniformMatrix4fv(u_model_view_matrix, false, localMatrizTransformacion);
        
        var vertexPositionAttribute = gl.getAttribLocation(glProgram, "aVertexPosition");
        gl.enableVertexAttribArray(vertexPositionAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_position_buffer);
        gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

        var vertexColorAttribute = gl.getAttribLocation(glProgram, "aVertexColor");
        gl.enableVertexAttribArray(vertexColorAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_color_buffer);
        gl.vertexAttribPointer(vertexColorAttribute, 3, gl.FLOAT, false, 0, 0);

        var vertexTexCoordAttribute = gl.getAttribLocation(glProgram, "aTexCoord");
        gl.enableVertexAttribArray(vertexTexCoordAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_textura_buffer);
        gl.vertexAttribPointer(vertexTexCoordAttribute, 2, gl.FLOAT, false, 0, 0);

        var vertexNormalAttribute = gl.getAttribLocation(glProgram, "aVertexNormal");
        gl.enableVertexAttribArray(vertexNormalAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_normal_buffer);
        gl.vertexAttribPointer(vertexNormalAttribute, 3, gl.FLOAT, false, 0, 0);

        if(this.tangent_buffer) {
            var vertexTangentAttribute = gl.getAttribLocation(glProgram, "aVertexTangent");
            gl.enableVertexAttribArray(vertexTangentAttribute);
            gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_tangent_buffer);
            gl.vertexAttribPointer(vertexTangentAttribute, 3, gl.FLOAT, false, 0, 0);
        }

        if(this.texturas) {
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, this.texturas[0]);
            gl.uniform1i(gl.getUniformLocation(glProgram, "uTexDiffuse"), 0);
            
            gl.activeTexture(gl.TEXTURE1);
            gl.bindTexture(gl.TEXTURE_2D, this.texturas[1]);
            gl.uniform1i(gl.getUniformLocation(glProgram, "uTexNorm"), 1);
        }

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.webgl_index_buffer);

        // Dibujamos.
        gl.drawElements(gl.TRIANGLE_STRIP, this.index_buffer.length, gl.UNSIGNED_SHORT, 0);

    }

    this.drawLine = function(matrizTransformacion, noStrip) {

        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);

        var nMatrix = mat3.create();
        var u_model_view_matrix = gl.getUniformLocation(glProgram, "umvMatrix");
        var u_normal_matrix = gl.getUniformLocation(glProgram, "uNMatrix");
        mat3.normalFromMat4(nMatrix, localMatrizTransformacion);
        gl.uniformMatrix3fv(u_normal_matrix, false, nMatrix);
        gl.uniformMatrix4fv(u_model_view_matrix, false, localMatrizTransformacion);
        
        var vertexPositionAttribute = gl.getAttribLocation(glProgram, "aVertexPosition");
        gl.enableVertexAttribArray(vertexPositionAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_position_buffer);
        gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

        var vertexColorAttribute = gl.getAttribLocation(glProgram, "aVertexColor");
        gl.enableVertexAttribArray(vertexColorAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_color_buffer);
        gl.vertexAttribPointer(vertexColorAttribute, 3, gl.FLOAT, false, 0, 0);

        var vertexNormalAttribute = gl.getAttribLocation(glProgram, "aVertexNormal");
        gl.enableVertexAttribArray(vertexNormalAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.webgl_normal_buffer);
        gl.vertexAttribPointer(vertexNormalAttribute, 3, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.webgl_index_buffer);

        // Dibujamos.
        if(noStrip) {
            gl.drawElements(gl.LINES, this.index_buffer.length, gl.UNSIGNED_SHORT, 0);
        } else {
            gl.drawElements(gl.LINE_STRIP, this.index_buffer.length, gl.UNSIGNED_SHORT, 0);
        }

    }

}
