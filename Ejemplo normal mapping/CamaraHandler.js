function CamaraHandler() {

    this.matrizVista = mat4.create();
    var modo = null;
    
    var factorVelocidad = 0.1;

    var mouse = {

        ultimaPosicionX: 0.0,
        ultimaPosicionY: 0.0,
        estaPresionado: false,
        factorVelocidad: 0.01

    }
    
    var camaraOrbital = {

        centro: [0.0, 0.0, 0.0], // A donde miro
        radio: 10.0,
        alfa: 0.0,
        beta: Math.PI/2.0,
        posicion: [0.0, 0.0, 0.0],
        up: [0.0, 1.0, 0.0]

    }

    var camaraPrimeraPersona = {

        posicion: [0.0, -5.0, -15.0],
        alfa: 0.0,
        beta: 0.0

    }

    this.configurarHandlers = function(body, canvas) {

        body.addEventListener("keydown", this.camaraOnKeyDown.bind(this), false);
        body.addEventListener("mouseup", this.camaraOnMouseUp.bind(this), false);
        canvas.addEventListener("mousedown", this.camaraOnMouseDown.bind(this), false);
        canvas.addEventListener("mousemove", this.camaraOnMouseMove.bind(this), false);
        canvas.addEventListener("wheel", this.camaraOnWheel.bind(this), false);

    }

    this.configurarCamaraOrbital = function(centro) {

        if(centro) {
            camaraOrbital.centro[0] = centro[0];
            camaraOrbital.centro[1] = centro[1];
            camaraOrbital.centro[2] = centro[2];
        }

        modo = "orbital";

        this.matrizVista = calcularMatrizVista();

    }

    this.configurarCamaraPrimeraPersona = function() {
        
        modo = "primera_persona";

        this.matrizVista = calcularMatrizVista();

    }

    this.camaraOnKeyDown = function(e) {
        
        switch (e.keyCode) {

            case 107:		// '+'
                if(modo === "orbital") {
                    camaraOrbital.radio += -0.1;
                    if (camaraOrbital.radio < 0) {
                        camaraOrbital.radio = 0;
                    }
                    this.matrizVista = calcularMatrizVista();
                }
                break;

            case 109:		// '-'
                if(modo === "orbital") {
                    camaraOrbital.radio += 0.1;
                    if (camaraOrbital.radio < 0) {
                        camaraOrbital.radio = 0;
                    }
                    this.matrizVista = calcularMatrizVista();
                }
                break;

            case 49:		// '1'
                this.configurarCamaraOrbital();
                break;

            // case 50:		// '2'
            //     this.configurarCamaraOrbital(otraposicion);
            //     break;

            // case 51:		// '3'
            //     this.configurarCamaraOrbital(otraposicion);
            //     break;

            // case 52:		// '4'
            //     this.configurarCamaraOrbital(otraposicion);
            //     break;

            case 53:		// '5'
                if(modo === "orbital") {
                    this.configurarCamaraPrimeraPersona();
                }
                break;

            case 87:
            case 38:			// 'W' o 'ArrowUp'
                camaraPrimeraPersona.posicion[2] += Math.cos(camaraPrimeraPersona.alfa) * factorVelocidad;
                camaraPrimeraPersona.posicion[0] += Math.sin(camaraPrimeraPersona.alfa) * factorVelocidad;
                this.matrizVista = calcularMatrizVista();
                break;

            case 65:
            case 37:			// 'A' o 'ArrowLeft'
                camaraPrimeraPersona.posicion[2] += Math.cos(camaraPrimeraPersona.alfa + Math.PI/2) * factorVelocidad;
                camaraPrimeraPersona.posicion[0] += Math.sin(camaraPrimeraPersona.alfa + Math.PI/2) * factorVelocidad;
                this.matrizVista = calcularMatrizVista();
                break;

            case 83:
            case 40:			// 'S' o 'ArrowDown'
                camaraPrimeraPersona.posicion[2] -= Math.cos(camaraPrimeraPersona.alfa) * factorVelocidad;
                camaraPrimeraPersona.posicion[0] -= Math.sin(camaraPrimeraPersona.alfa) * factorVelocidad;
                this.matrizVista = calcularMatrizVista();
                break;

            case 68:
            case 39:			// 'D' o 'ArrowRight'
                camaraPrimeraPersona.posicion[2] += Math.cos(camaraPrimeraPersona.alfa - Math.PI/2) * factorVelocidad;
                camaraPrimeraPersona.posicion[0] += Math.sin(camaraPrimeraPersona.alfa - Math.PI/2) * factorVelocidad;
                this.matrizVista = calcularMatrizVista();
                break;

            case 81:			// 'Q'
                camaraPrimeraPersona.posicion[1] -= factorVelocidad;
                this.matrizVista = calcularMatrizVista();
                break;
            case 69:			// 'E'
                camaraPrimeraPersona.posicion[1] += factorVelocidad;
                this.matrizVista = calcularMatrizVista();
    break;

        }

    }

    this.camaraOnMouseDown = function(e) {

        if(modo === "orbital") {
            mouse.ultimaPosicionX = e.clientX;
            mouse.ultimaPosicionY = e.clientY;

            mouse.estaPresionado = true;
        }

    }

    this.camaraOnMouseMove = function(e) {

        if(modo === "orbital" && mouse.estaPresionado) {

            var nuevaPosicionX = e.clientX, nuevaPosicionY = e.clientY;
            
            var deltaX = nuevaPosicionX - mouse.ultimaPosicionX, deltaY = nuevaPosicionY - mouse.ultimaPosicionY;

            camaraOrbital.alfa -= deltaX * mouse.factorVelocidad;
            var beta = camaraOrbital.beta - deltaY * mouse.factorVelocidad;
    
            if (beta > 0 && beta < Math.PI) {
                camaraOrbital.beta = beta;
            }

            mouse.ultimaPosicionX = nuevaPosicionX; mouse.ultimaPosicionY = nuevaPosicionY;

            this.matrizVista = calcularMatrizVista();

        } else if(modo === "primera_persona") {

            var nuevaPosicionX = e.clientX, nuevaPosicionY = e.clientY;
            
            var deltaX = nuevaPosicionX - mouse.ultimaPosicionX, deltaY = nuevaPosicionY - mouse.ultimaPosicionY;

            camaraPrimeraPersona.alfa -= deltaX * mouse.factorVelocidad;
            var beta = camaraPrimeraPersona.beta - deltaY * mouse.factorVelocidad;
    
            if (beta > -Math.PI/2 && beta < Math.PI/2) {
                camaraPrimeraPersona.beta = beta;
            }

            mouse.ultimaPosicionX = nuevaPosicionX; mouse.ultimaPosicionY = nuevaPosicionY;

            this.matrizVista = calcularMatrizVista();

        }

    }

    this.camaraOnMouseUp = function(e) {

        mouse.estaPresionado = false;

    }

    this.camaraOnWheel = function(e) {

        e.preventDefault();
        
        if(modo === "orbital") {
            (e.deltaY < 0) ? camaraOrbital.radio += -0.1 : camaraOrbital.radio += 0.1;

            if (camaraOrbital.radio < 0) {
                camaraOrbital.radio = 0;
            }
        }

        this.matrizVista = calcularMatrizVista();

    }

    var calcularMatrizVista = function() {

        var matrizVista = mat4.create();

        if (modo === "orbital") {

            camaraOrbital.posicion = vec3.fromValues(
                camaraOrbital.radio * Math.sin(camaraOrbital.alfa) * Math.sin(camaraOrbital.beta),
                camaraOrbital.radio * Math.cos(camaraOrbital.beta),
                camaraOrbital.radio * Math.cos(camaraOrbital.alfa) * Math.sin(camaraOrbital.beta)
            );

            vec3.add(camaraOrbital.posicion, camaraOrbital.posicion, camaraOrbital.centro);
            vec3.add(camaraOrbital.posicion, camaraOrbital.posicion, [1e-6, 0.0, 0.0]);

            mat4.lookAt(matrizVista, camaraOrbital.posicion, camaraOrbital.centro, camaraOrbital.up);

            return matrizVista;
            
        } else if(modo === "primera_persona") {

            mat4.rotate(matrizVista, matrizVista, camaraPrimeraPersona.beta, [-1.0, 0.0, 0.0]);
			mat4.rotate(matrizVista, matrizVista, camaraPrimeraPersona.alfa, [0.0, -1.0, 0.0]);
            mat4.translate(matrizVista, matrizVista, camaraPrimeraPersona.posicion);
            
            return matrizVista;

        }

    }


}
