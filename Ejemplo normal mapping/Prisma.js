function Prisma(ancho, profundidad, alto, tapaInicial, tapaFinal) {

    this.posicion = vec3.create();
    this.angulosEjes = vec3.create();
    this.escalasEjes = vec3.fromValues(1, 1, 1);
    
    var rectangulo = new Rectangulo(profundidad, ancho);
    var segmento = new CurvaSegmentada([ [0, -alto/2, 0], [0, alto/2, 0] ], 2);
    var prisma = new SuperficieBarrido(segmento, rectangulo, tapaInicial, tapaFinal);

    this.dibujar = function(matrizTransformacion) {

        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);

        prisma.dibujar(localMatrizTransformacion);

    }

}
