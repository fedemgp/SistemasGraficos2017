function Cilindro() {

    var altura = 1;
    var cols = 10;
    var radio = 1;

    this.posicion = vec3.create();
    this.angulosEjes = vec3.create();
    this.escalasEjes = vec3.fromValues(1, 1, 1);
    
    var grilla = new VertexGrid();

    grilla.position_buffer = [];
    grilla.color_buffer = [];
    grilla.normal_buffer = [];
    grilla.textura_buffer = [];

    var angulo_barrido = (2*Math.PI)/(cols-1);

    for (var i = 0.0; i < (altura + 1); i++) {
            
            for (var j = 0.0; j < cols; j++) {

                var vertice_angulo = j*angulo_barrido;
                //
                grilla.position_buffer.push(radio*Math.sin(vertice_angulo));
                grilla.position_buffer.push(altura*i);
                grilla.position_buffer.push(radio*Math.cos(vertice_angulo));

                // Para cada vértice definimos su color
                grilla.color_buffer.push(1.0/altura * i);
                grilla.color_buffer.push(0.2);
                grilla.color_buffer.push(1.0/cols * j);

                var normal_vertice = vec3.fromValues(
                    radio*Math.sin(vertice_angulo),
                    0,
                    radio*Math.cos(vertice_angulo)
                );
                vec3.normalize(normal_vertice, normal_vertice);
                grilla.normal_buffer.push(normal_vertice[0]);
                grilla.normal_buffer.push(normal_vertice[1]);
                grilla.normal_buffer.push(normal_vertice[2]);

                grilla.textura_buffer.push(i/(altura));
                grilla.textura_buffer.push(vertice_angulo/(2*Math.PI));
                                        
            }
    }

    grilla.createIndexBuffer(altura + 1, cols);
    grilla.setupWebGLBuffers();

    this.dibujar = function(matrizTransformacion) {

        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);
        grilla.draw(localMatrizTransformacion);

    }

}
