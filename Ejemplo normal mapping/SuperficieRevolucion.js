function SuperficieRevolucion(curvaForma, ejeRevolucion, resolucion, tapaInicial, tapaFinal) {

    this.posicion = vec3.create();
    this.angulosEjes = vec3.create();
    this.escalasEjes = vec3.fromValues(1, 1, 1);
    
    var curvaForma = curvaForma;
    var ejeRevolucion = ejeRevolucion;
    var resolucion = resolucion;

    var grilla = new VertexGrid();

    grilla.position_buffer = [];
    grilla.color_buffer = [];
    grilla.normal_buffer = [];
    grilla.tangent_buffer = [];
    grilla.textura_buffer = [];

    var tapar = function(punto, direccionTangente) { 
        
        for(var i = 0; i<curvaForma.puntos.length; i++) {

            grilla.position_buffer.push(punto[0]);
            grilla.position_buffer.push(punto[1]);
            grilla.position_buffer.push(punto[2]);
            grilla.normal_buffer.push(punto[0]);
            grilla.normal_buffer.push(punto[1]);
            grilla.normal_buffer.push(punto[2]);
            grilla.tangent_buffer.push(punto[0]);
            grilla.tangent_buffer.push(punto[1]);
            grilla.tangent_buffer.push(punto[2]);
            grilla.color_buffer.push(1.0);
            grilla.color_buffer.push(0.2);
            grilla.color_buffer.push(1.0);
            grilla.textura_buffer.push(0.0);
            grilla.textura_buffer.push(0.0);

        }

    }

    this.dibujar = function(matrizTransformacion, line) {//console.log(curvaForma.puntos);console.log(grilla.position_buffer);

        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);

        if(line) {
            grilla.drawLine(localMatrizTransformacion);
        } else {
            grilla.draw(localMatrizTransformacion);
        }
        // b = new Curva("bezier", [ [0,0] , [1,4] , [4,4] , [5,8] ], 20);
        // for(var i = 0; i<resolucion; i++) {
        //    var a = new VertexGrid(), c = new VertexGrid();
        //    a.position_buffer = [];
        //    a.color_buffer = [];
        //    a.normal_buffer = [];
        //    a.textura_buffer = [];
        //    c.position_buffer = [];
        //    c.color_buffer = [];
        //    c.normal_buffer = [];
        //    c.textura_buffer = [];
        //    var matrizRotacion = mat4.create();
        //    mat4.identity(matrizRotacion, matrizRotacion);
        //             mat4.rotate(matrizRotacion, matrizRotacion, i * 2*Math.PI/resolucion, ejeRevolucion);
        //             // b.posicion=[5, 0, 0];
        //     // b.angulosEjes = [0, i * 2*Math.PI/resolucion, 0];
        //     // b.dibujar(localMatrizTransformacion);
        //             for(var j = 0; j<curvaForma.puntos.length; j++) {//console.log(i);
            
        //                 var punto = vec3.create();
        //                 vec3.copy(punto, curvaForma.puntos[j]);
        //                 var normal = vec3.create();
        //                 vec3.copy(normal, curvaForma.normales[j]);
            
        //                 vec3.transformMat4(punto, punto, matrizRotacion);//console.log(punto);
        //                 vec3.transformMat4(normal, normal, matrizRotacion);
            
        //                 a.position_buffer.push(punto[0]);
        //                 a.position_buffer.push(punto[1]);
        //                 a.position_buffer.push(punto[2]);
        //                 a.normal_buffer.push(punto[0]);
        //                 a.normal_buffer.push(punto[1]);
        //                 a.normal_buffer.push(punto[2]);
        //                 a.color_buffer.push(1.0/curvaForma.length);
        //                 a.color_buffer.push(0.2);
        //                 a.color_buffer.push(1.0/curvaForma.length);

        //                 var punto = vec3.create();
        //                 vec3.copy(punto, b.puntos[j]);
        //                 var normal = vec3.create();
        //                 vec3.copy(normal, b.normales[j]);
            
        //                 vec3.transformMat4(punto, punto, matrizRotacion);//console.log(punto);
        //                 vec3.transformMat4(normal, normal, matrizRotacion);
            
        //                 c.position_buffer.push(punto[0]);
        //                 c.position_buffer.push(punto[1]);
        //                 c.position_buffer.push(punto[2]);
        //                 c.normal_buffer.push(punto[0]);
        //                 c.normal_buffer.push(punto[1]);
        //                 c.normal_buffer.push(punto[2]);
        //                 c.color_buffer.push(1.0/curvaForma.length);
        //                 c.color_buffer.push(0.2);
        //                 c.color_buffer.push(1.0/curvaForma.length);
            
        //             }//console.log(a.position_buffer);
        //             a.createIndexBuffer(1, curvaForma.puntos.length);
        //             a.setupWebGLBuffers();
        //             // a.drawLine(localMatrizTransformacion);
        //             c.createIndexBuffer(1, b.puntos.length);
        //             c.setupWebGLBuffers();
        //             // c.drawLine(localMatrizTransformacion);
            
        //         }

    }

    grilla.createIndexBuffer(resolucion + 1 + 1*tapaInicial + 1*tapaFinal, curvaForma.puntos.length);

    // if(tapaInicial) {
    //     tapar(curvaCamino.puntos[0], -1.0);
    // }

    var matrizRotacion = mat4.create();
    
    for(var i = 0; i<resolucion; i++) {//console.log(i * 2*Math.PI/resolucion + " " + (i * 2*Math.PI/resolucion - (i-1) * 2*Math.PI/resolucion));

        mat4.identity(matrizRotacion, matrizRotacion);
        mat4.rotate(matrizRotacion, matrizRotacion, i * 2*Math.PI/resolucion, ejeRevolucion);

        for(var j = 0; j<curvaForma.puntos.length; j++) {

            var punto = vec3.create();
            vec3.copy(punto, curvaForma.puntos[j]);
            var normal = vec3.create();
            vec3.copy(normal, curvaForma.normales[j]);

            vec3.transformMat4(punto, punto, matrizRotacion);
            vec3.transformMat4(normal, normal, matrizRotacion);

            grilla.position_buffer.push(punto[0]);
            grilla.position_buffer.push(punto[1]);
            grilla.position_buffer.push(punto[2]);
            grilla.normal_buffer.push(punto[0]);
            grilla.normal_buffer.push(punto[1]);
            grilla.normal_buffer.push(punto[2]);
            grilla.color_buffer.push(1.0/curvaForma.length * i);
            grilla.color_buffer.push(0.2);
            grilla.color_buffer.push(1.0/curvaForma.length * j);
            grilla.tangent_buffer.push(punto[0]);
            grilla.tangent_buffer.push(punto[1]);
            grilla.tangent_buffer.push(punto[2]);
            grilla.textura_buffer.push(i/(resolucion - 1));
            grilla.textura_buffer.push(j/(curvaForma.puntos.length - 1));

        }

    }

    for(var i = 0; i<curvaForma.puntos.length; i++) {
    
        for(var j = 0; j<3; j++) {//console.log("dsa");console.log(grilla.position_buffer);

            grilla.position_buffer.push(grilla.position_buffer[3*i + j]);
            grilla.normal_buffer.push(grilla.normal_buffer[3*i + j]);
            grilla.color_buffer.push(grilla.color_buffer[3*i + j]);
            grilla.tangent_buffer.push(grilla.tangent_buffer[3*i + j]);

        }

        grilla.textura_buffer.push(2*i);
        grilla.textura_buffer.push(2*i + 1);

    }

    // if(tapaFinal) {
    //     tapar(curvaCamino.puntos[curvaCamino.puntos.length - 1], 1.0);
    // }

    grilla.setupWebGLBuffers();

}
