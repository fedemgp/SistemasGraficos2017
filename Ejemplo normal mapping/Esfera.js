function Esfera() {

    var rows = 50;
    var cols = 50;
    var radio = 1;
    
    var grilla = new VertexGrid();

    this.posicion = vec3.create();
    this.angulosEjes = vec3.create();
    this.escalasEjes = vec3.fromValues(1, 1, 1);

    grilla.position_buffer = [];
    grilla.color_buffer = [];
    grilla.normal_buffer = [];
    grilla.textura_buffer = [];

    var angulo_phi_barrido = (2*Math.PI)/(cols-1);
    var angulo_theta_barrido = Math.PI/(rows-1);

    for (var i = 0.0; i < rows; i++) { 
        
        var vertice_angulo_theta = i*angulo_theta_barrido;
        
        for (var j = 0.0; j < cols; j++) {

            var vertice_angulo_phi = j*angulo_phi_barrido;
            //
            grilla.position_buffer.push(radio*Math.sin(vertice_angulo_theta)*Math.cos(vertice_angulo_phi));
            grilla.position_buffer.push(radio*Math.sin(vertice_angulo_theta)*Math.sin(vertice_angulo_phi));
            grilla.position_buffer.push(radio*Math.cos(vertice_angulo_theta));

            // Para cada vértice definimos su color
            grilla.color_buffer.push(1.0/rows * i);
            grilla.color_buffer.push(0.2);
            grilla.color_buffer.push(1.0/cols * j);

            var normal_vertice = vec3.fromValues(
                radio*Math.sin(vertice_angulo_theta)*Math.cos(vertice_angulo_phi),
                radio*Math.sin(vertice_angulo_theta)*Math.sin(vertice_angulo_phi),
                radio*Math.cos(vertice_angulo_theta)
            );
            vec3.normalize(normal_vertice, normal_vertice);
            grilla.normal_buffer.push(normal_vertice[0]);
            grilla.normal_buffer.push(normal_vertice[1]);
            grilla.normal_buffer.push(normal_vertice[2]);

            grilla.textura_buffer.push(vertice_angulo_theta/Math.PI);
            grilla.textura_buffer.push(vertice_angulo_phi/(2*Math.PI));
        }
    }

    grilla.createIndexBuffer(rows, cols);
    grilla.setupWebGLBuffers();

    this.dibujar = function(matrizTransformacion) {

        var localMatrizTransformacion = mat4.create();
        var localviewMatriz = mat4.create();
        mat4.copy(localviewMatriz, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);

        grilla.draw(localMatrizTransformacion, localviewMatriz);

    }

}
