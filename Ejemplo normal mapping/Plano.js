function Plano(texturas) {

    var largo = 10; // eje x
    var ancho = 10; // eje y
    
    var grilla = new VertexGrid();

    this.posicion = vec3.create();
    this.angulosEjes = vec3.create();
    this.escalasEjes = vec3.fromValues(1, 1, 1);

    grilla.position_buffer = [];
    grilla.color_buffer = [];
    grilla.normal_buffer = [];
    grilla.tangent_buffer = [];
    grilla.textura_buffer = [];

    for (var i = 0.0; i < (largo + 1); i++) { 
            
        for (var j = 0.0; j < (ancho + 1); j++) {

            // Para cada vértice definimos su posición
            // como coordenada (x, y, z=0)
            grilla.position_buffer.push(i-largo/2.0);
            grilla.position_buffer.push(j-ancho/2.0);
            grilla.position_buffer.push(0);

            // Para cada vértice definimos su color
            grilla.color_buffer.push(1.0/largo * i);
            grilla.color_buffer.push(0.2);
            grilla.color_buffer.push(1.0/ancho * j);

            grilla.normal_buffer.push(0);
            grilla.normal_buffer.push(0);
            grilla.normal_buffer.push(1);

            grilla.tangent_buffer.push(1);
            grilla.tangent_buffer.push(0);
            grilla.tangent_buffer.push(0);

            grilla.textura_buffer.push(i/largo);
            grilla.textura_buffer.push(j/ancho);
                                    
        }
        
    }

    if(texturas) {
        grilla.texturas = [];
        grilla.texturas.push(texturas[0]);
        grilla.texturas.push(texturas[1]);
    }

    grilla.createIndexBuffer(largo + 1, ancho + 1);
    grilla.setupWebGLBuffers();

    this.dibujar = function(matrizTransformacion) {

        var localMatrizTransformacion = mat4.create();
        var localviewMatriz = mat4.create();
        mat4.copy(localviewMatriz, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);
      
        grilla.draw(localMatrizTransformacion, localviewMatriz);

    }

}
