function CurvaSegmentada(puntosControl, cantidadPuntosPorSegmento) {

    this.puntos = [];
    this.normales = [];
    this.tangentes = [];
    this.posicion = vec3.create();
    this.angulosEjes = vec3.create();
    this.escalasEjes = vec3.fromValues(1, 1, 1);

    var grilla = new VertexGrid();

    grilla.position_buffer = [];
    grilla.color_buffer = [];
    grilla.normal_buffer = [];
    grilla.textura_buffer = [];
    
    var puntosControl = puntosControl;
    var deltaU = 1/(cantidadPuntosPorSegmento-1);
    var cantidadSegmentos = puntosControl.length - 1;
    var avance = 1;

    this.en = function(u, p0, p1) {
        
        var punto = vec3.create();
        
        punto[0] = p0[0] + u*(p1[0] - p0[0]);
        punto[1] = p0[1] + u*(p1[1] - p0[1]);
        punto[2] = p0[2] + u*(p1[2] - p0[2]);

        return punto;

    }

    this.tangenteEn = function(u, p0, p1) {

        var tangente = vec3.create();
        
        tangente[0] = p1[0] - p0[0];
        tangente[1] = p1[1] - p0[1];
        tangente[2] = p1[2] - p0[2];
        vec3.normalize(tangente, tangente);

        return tangente;

    }

    this.normalEn = function(u, p0, p1) {

        var tangente = this.tangenteEn(u, p0, p1);
        var normal = vec3.fromValues(tangente[0] + 1.0, tangente[1], tangente[2]);

        vec3.cross(normal, tangente, normal);
        vec3.cross(normal, normal, tangente);
        vec3.normalize(normal, normal);

        return normal;

    }

    this.dibujar = function(matrizTransformacion) {
        
        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);        
        
        grilla.drawLine(localMatrizTransformacion);

    }

    this.dibujarNormales = function(matrizTransformacion) {

        var grillaNormales = new VertexGrid();

        grillaNormales.position_buffer = [];
        grillaNormales.color_buffer = [];
        grillaNormales.normal_buffer = [];
        grillaNormales.textura_buffer = [];

        for(var i = 0; i<this.puntos.length; i++) {
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+1]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+2]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3] + grilla.normal_buffer[i*3]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+1] + grilla.normal_buffer[i*3+1]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+2] + grilla.normal_buffer[i*3+2]);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(1);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(1);
        }
        
        grillaNormales.createIndexBuffer(1, 2*this.puntos.length);
        grillaNormales.setupWebGLBuffers();
        
        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);

        grillaNormales.drawLine(localMatrizTransformacion, true);

    }

    var posicion = 0;

    for(var i = 0; i<cantidadSegmentos; i++) {

        var p0 = puntosControl[posicion];
        var p1 = puntosControl[posicion+1];

        posicion += avance;
        
        for(var u = 0.0; u<=1.001; u += deltaU) {

            this.puntos.push(this.en(u, p0, p1));
            this.normales.push(this.normalEn(u, p0, p1));
            this.tangentes.push(this.tangenteEn(u, p0, p1));

        }

    }

    grilla.createIndexBuffer(1, this.puntos.length);

    for(var i = 0; i<this.puntos.length; i++) {
        grilla.position_buffer.push(this.puntos[i][0]);
        grilla.position_buffer.push(this.puntos[i][1]);
        grilla.position_buffer.push(this.puntos[i][2]);
        grilla.color_buffer.push(0.1);
        grilla.color_buffer.push(0.1);
        grilla.color_buffer.push(1.0);
        grilla.normal_buffer.push(this.normales[i][0]);
        grilla.normal_buffer.push(this.normales[i][1]);
        grilla.normal_buffer.push(this.normales[i][2]);
    }

    grilla.setupWebGLBuffers();


}
