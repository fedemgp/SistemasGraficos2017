function Circunferencia(radio) {

    const PUNTOS_CIRCULO = 40;
    
    this.puntos = [];
    this.normales = [];
    this.tangentes = [];
    this.posicion = vec3.create();
    this.angulosEjes = vec3.create();
    this.escalasEjes = vec3.fromValues(1, 1, 1);

    var grilla = new VertexGrid();

    grilla.position_buffer = [];
    grilla.color_buffer = [];
    grilla.normal_buffer = [];
    grilla.textura_buffer = [];

    var radio = radio;
    var deltaU = 1/(PUNTOS_CIRCULO - 1);

    this.en = function(u) {

        return vec3.fromValues(radio*Math.cos(2*Math.PI*u), radio*Math.sin(2*Math.PI*u), 0.0);

    }

    this.tangenteEn = function(u) {

        var tangente = vec3.fromValues(-Math.sin(2*Math.PI*u), Math.cos(2*Math.PI*u), 0.0);
        vec3.normalize(tangente, tangente);

        return tangente;

    }
    
    this.normalEn = function(u) {

        var normal = vec3.fromValues(Math.cos(2*Math.PI*u), Math.sin(2*Math.PI*u), 0.0);
        vec3.normalize(normal, normal);

        return normal;

    }

    this.dibujar = function(matrizTransformacion) {
        
        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);        
        
        grilla.drawLine(localMatrizTransformacion);

    }

    this.dibujarNormales = function(matrizTransformacion) {

        var grillaNormales = new VertexGrid();

        grillaNormales.position_buffer = [];
        grillaNormales.color_buffer = [];
        grillaNormales.normal_buffer = [];
        grillaNormales.textura_buffer = [];

        for(var i = 0; i<this.puntos.length; i++) {
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+1]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+2]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3] + grilla.normal_buffer[i*3]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+1] + grilla.normal_buffer[i*3+1]);
            grillaNormales.position_buffer.push(grilla.position_buffer[i*3+2] + grilla.normal_buffer[i*3+2]);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.color_buffer.push(0);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(1);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(0);
            grillaNormales.normal_buffer.push(1);
        }
        
        grillaNormales.createIndexBuffer(1, 2*this.puntos.length);
        grillaNormales.setupWebGLBuffers();
        
        var localMatrizTransformacion = mat4.create();
        mat4.copy(localMatrizTransformacion, matrizTransformacion);
        mat4.translate(localMatrizTransformacion, localMatrizTransformacion, this.posicion);
        mat4.rotateX(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[0]);
        mat4.rotateY(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[1]);
        mat4.rotateZ(localMatrizTransformacion, localMatrizTransformacion, this.angulosEjes[2]);
        mat4.scale(localMatrizTransformacion, localMatrizTransformacion, this.escalasEjes);

        grillaNormales.drawLine(localMatrizTransformacion, true);

    }
    
    for(var i = 0, u = 0; i<PUNTOS_CIRCULO; i++) {

        this.puntos.push(this.en(u));
        this.normales.push(this.normalEn(u));
        this.tangentes.push(this.tangenteEn(u));

        u += deltaU;

    }

    grilla.createIndexBuffer(1, this.puntos.length);
    
    for(var i = 0; i<this.puntos.length; i++) {
        grilla.position_buffer.push(this.puntos[i][0]);
        grilla.position_buffer.push(this.puntos[i][1]);
        grilla.position_buffer.push(this.puntos[i][2]);
        grilla.color_buffer.push(0.1);
        grilla.color_buffer.push(0.1);
        grilla.color_buffer.push(1.0);
        grilla.normal_buffer.push(this.normales[i][0]);
        grilla.normal_buffer.push(this.normales[i][1]);
        grilla.normal_buffer.push(this.normales[i][2]);
    }

    grilla.setupWebGLBuffers();

}
